from flask import Flask
from flask import request
from flask import jsonify

import tg_lib
import datetime
import json
import requests
from collections import deque



app = Flask(__name__)
last_messages = deque(maxlen=10)

@app.route('/', methods=['POST','GET'])
def index():
	print("ZAPROS")
	if request.method == 'POST':
		r = request.get_json()
		print(r)
		chat = r['message']['chat']['id']
		userText = r['message']['text']

		id = r['update_id']
		date = datetime.datetime.utcfromtimestamp(r['message']['date']).strftime('%Y-%m-%d %H:%M:%S')
		name = r['message']['chat']['first_name']

		last_messages.append(f'{id},{date},{name}')

		if userText == '/online':
			tg.send_message(chat,'I am online')
		elif userText == '/last':
			tg.send_message(chat, json.dumps([i for i in last_messages]))
		elif userText == '/curs':
			eur = requests.get('https://www.nbrb.by/API/ExRates/Rates/292')
			eur = eur.json()['Cur_OfficialRate']
			usd = requests.get('https://www.nbrb.by/API/ExRates/Rates/145')
			usd = usd.json()['Cur_OfficialRate']
			tg.send_message(chat,f'НБРБ Евро:{eur}, Доллар: {usd}')
		else:
			tg.send_message(chat,'Are you sure you wanted to say:')
			tg.send_message(chat,r['message']['text'])

		return jsonify(r)

	return '<h1>Bot is up and running</h1>'


if __name__=='__main__':
	app.run(host='0.0.0.0', port=8443, debug=False)
