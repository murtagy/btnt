import requests
import datetime
import json
from time import sleep

class BotHandler():
    def __init__(self, token):
        self.token = token
        self.api_url = "https://api.telegram.org/bot" + token
        self.update_rate_counter = 10
        self.last_update_id = self.get_updates()['result'][-1]['update_id']
        self.main()


    def get_updates(self):
        method='/getUpdates'
        r = requests.get(''.join([self.api_url,method]))
        assert r.ok, 'BAD REQUEST'
        return r.json()

    def get_feed(self):
        feed = self.get_updates()
        feed = feed['result']
        feed_formated = []
        for i in list(reversed(feed))[0:10]:
            id = i['update_id']
            date = datetime.datetime.utcfromtimestamp(i['message']['date']).strftime('%Y-%m-%d %H:%M:%S')
            name = i['message']['chat']['first_name']
            feed_formated.append({"id":id, "date":date, "name":name})
        return json.dumps(feed_formated)

    def send_message(self, chat, text):
    	url = f'{self.api_url}/sendmessage?chat_id={chat}&text={text}'
    	r = requests.post(url)
    	return r

    def main(self):
        while True:
        	if self.update_rate_counter < 10: #telegram timeout seem not work
        		sleep(1)
        	else:
        		sleep(10)

        	update_request = self.get_updates()
        	r = update_request['result'][-1]
        	last_update_id = r['update_id']

        	if self.last_update_id == last_update_id:
        		self.update_rate_counter += 1
        		print('No activity',self.update_rate_counter)
        		pass
        	else:
        		print('Got message!')
        		self.update_rate_counter = 0
        		self.last_update_id = last_update_id
        		chat = r['message']['chat']['id']
        		command = r['message']['text']

        		if command == '/last':
        			feed = self.get_feed()
        			self.send_message(chat,text=feed)
        		elif command == '/online':
        			self.send_message(chat,text='I am online')
        		elif command ==  '/curs':
        			eur = requests.get('https://www.nbrb.by/API/ExRates/Rates/292')
        			eur = eur.json()['Cur_OfficialRate']
        			usd = requests.get('https://www.nbrb.by/API/ExRates/Rates/145')
        			usd = usd.json()['Cur_OfficialRate']
        			self.send_message(chat,f'НБРБ Евро:{eur}, Доллар: {usd}')
        		else:
        			self.send_message(chat,'Wrong command!')


if __name__ == '__main__':
    BotHandler('738667871:AAEpxp25peS0fDynBHfxyHtzhCFHmjLi3s8')
